<?php

/*
 * A simple way to use library. You should make an instance from project and use the following methods
 * You should set something and then, you can send a request.
 * All requests will send with curl ext.
 * All parameters will defined in $params
 * Hope you enjoy it :)
 * written by : Matin Noroozi - 100ztaa@gmail.com
 * */

namespace Src;
require dirname(__DIR__) . "/vendor/autoload.php";

use Config\Database;

class MyTelegramSender
{
    public $userStatus = ['administrator', 'creator', 'member'];
    public $command;
    public $input;
    public $userName;
    public $firstName;
    public $callbackId;
    public $userId;
    public $chatId;
    public $messageId;
    public $params;
    public $lastName;
    public $userPhone;
    public $userLongitude;
    public $userLatitude;
    public $db;
    private $url;

    public function __set($name, $value)
    {
        if ($name == 'channel') {
            $this->$name = $value;
        }
    }

    public function __construct()
    {

        $this->db = new Database;
        $this->input = ($this->getUserContents('php://input', 1));
        $this->setUserData($this->input);
        $this->addContentsToFile('matin.txt', print_r($this->input, 1), FILE_APPEND);
    }

    function setText(array $input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendMessage';
        $this->userId = $input['user_id'] ?? $this->userId;
        $this->params = [
            'chat_id' => $this->userId,
            'text' => $input['text']
        ];

        $this->setButtons($input['buttons']);

        $this->setDisableStatusAndReply(@$input['disable_notification'], @$input['reply']);
    }

    function setVideo(array $input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendVideo';
        $this->params = [
            'chat_id' => $this->userId,
            'video' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setDuration($input['duration']);

        $this->setStreamingMod($input['stream_status']);
    }

    function setAnimation(array $input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendAnimation';
        $this->params = [
            'chat_id' => $this->userId,
            'animation' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setDuration($input['duration']);
    }

    function setVideoNote($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendVideoNote';
        $this->params = [
            'chat_id' => $this->userId,
            'video_note' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setLength($input['length']);

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setDuration($input['duration']);
    }

    function setAudio($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendAudio';
        $this->params = [
            'chat_id' => $this->userId,
            'audio' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setPerformer($input['performer']);

        $this->setTitle($input['title']);

        $this->setDuration($input['duration']);
    }

    function setVoice($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendVoice';
        $this->params = [
            'chat_id' => $this->userId,
            'voice' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setDuration($input['duration']);
    }


    function setDocument($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendDocument';
        $this->params = [
            'chat_id' => $this->userId,
            'document' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

        $this->setThumbnail($input['thumbnail']);
    }

    function setPhoto($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendPhoto';
        $this->params = [
            'chat_id' => $this->userId,
            'photo' => $input['file_id'],
            'caption' => $input['caption']
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);
    }


    function setLocation($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendLocation';
        $this->params = [
            'chat_id' => $this->userId,
            'latitude' => (float)$input['latitude'],
            'longitude' => (float)$input['longitude']
        ];

        $this->setLivePeriod($input['live_period']);

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);
    }

    function setVenue($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendVenue';
        $this->params = [
            'chat_id' => $this->userId,
            'latitude' => (float)$input['latitude'],
            'longitude' => (float)$input['longitude']
        ];


        $this->setAddress($input['address']);

        $this->setTitle($input['title']);

        $this->setFoursquare($input['foursquare_id'], $input['foursquare_type']);

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);

    }

    function setMediaGroup($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendMediaGroup';
        $this->params = [
            'chat_id' => $this->userId,
            'caption' => $input['caption'],
            'media' => json_encode($input['media'])
        ];

        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);
    }

    function setContact(array $input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendContact';
        $this->params = [
            'chat_id' => $this->userId,
            'phone_number' => $input['phone'],
            'first_name' => $input['first_name']
        ];

        $this->setLastName($input['last_name']);
        $this->setvCard($input['v-card']);
        $this->setDisableStatusAndReply($input['disable_notification'], $input['reply']);
    }

    function setPoll($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendPoll';
        $this->params = [
            'chat_id' => $this->userId,
            'question' => $input['question'],
            'options' => json_encode($input['options'])
        ];

        $this->setAnonymus($input['is_anonymous']);
        $this->setType($input['type']);
        $this->setallowsMultipleAnswers($input['allows_multiple_answers']);
        $this->setCorrectOptionId($input['correct_option_id']);
        $this->setIsClosed($input['is_closed']);
        $this->setDisableStatusAndReply($input['disableNot'], $input['reply']);
    }

    function setChatAction($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/sendChatAction';
        $this->params = [
            'chat_id' => $this->userId ?? 569942442,
            'action' => $input['action']
        ];

        $this->setDisableStatusAndReply($input['disableNot'], $input['reply']);
    }

    function getUserProfilePhotos($input = [])
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getUserProfilePhotos';
        $this->params = [
            'user_id' => $this->chatId,
        ];

        $this->setLimit($input['limit']);
        $this->setOffset($input['offset']);

    }


    // Methods for working files and photos
    function getFile($fileId)
    {

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getFile';
        if (is_string($fileId)) {
            $this->params = [
                'file_id' => $fileId,
            ];
        }
    }

    function getArrayPhotos(array $arrayOfPhotos)
    {
        $newArrayOfPhotos = [];
        for ($i = 0; $i < $arrayOfPhotos['result']['total_count']; $i++) {
            $newArrayOfPhotos[] = $arrayOfPhotos['result']['photos'][$i][0]['file_id'];
        }
        $this->addContentsToFile('photos.txt', print_r($newArrayOfPhotos, 1), FILE_APPEND);
        return $newArrayOfPhotos;
    }

    function setArrayPhotosGoodForMediaGroups(array $arrayPhotos)
    {
        $newMediaGrupArray = [];
        for ($i = 0; $i < count($arrayPhotos); $i++) {
            $newMediaGrupArray[] = [
                'type' => 'photo',
                'media' => $arrayPhotos[$i]
            ];
        }
        $this->addContentsToFile('photos.txt', print_r($newMediaGrupArray, 1));
        return $newMediaGrupArray;
    }

    // Group methods 
    public function setKickChatMember(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/kickChatMember';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId
        ];

        $this->setUntil_date($input['until_date']);
    }

    public function setUnbanChatMember(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/unbanChatMember';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId
        ];

    }

    public function setRestrictChatMember(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/restrictChatMember';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId
        ];
        $this->setUntil_date($input['until_date']);
        $this->setChatPermissions($input['permissions']);

    }

    public function setPromoteChatMember(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/promoteChatMember';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId
        ];

        $this->setCanChangInfo($input['can_change_info']);
        $this->setCanPostMessages($input['can_post_messages']);
        $this->setCanEditMessages($input['can_edit_messages']);
        $this->setCanDeleteMessages($input['can_delete_messages']);
        $this->setCanInviteUsers($input['can_invite_users']);
        $this->setCanRestrictMembers($input['can_restrict_members']);
        $this->setCanPinMessages($input['can_pin_messages']);
        $this->setCanPromoteMembers($input['can_promote_members']);

    }

    public function setChatAdministratorCustomTitle(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatAdministratorCustomTitle';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId,
            'custom_title' => $input['custom_title']
        ];
        var_dump($this->params);
    }

    public function setGroupPermissions(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatPermissions';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
        $this->setChatPermissions($input['permissions']);
        var_dump($this->params);
    }

    public function setExportChatInviteLink(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/exportChatInviteLink';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }


    public function setChatPhoto(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatPhoto';
        $this->params = [
            'chat_id' => $this->chatId,
            'photo' => $input['file_id']
        ];

    }

    public function deleteChatPhoto(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/deleteChatPhoto';
        $this->params = [
            'chat_id' => $this->chatId,
        ];

    }


    public function setChatTitle(array $input = []): void
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatTitle';
        $this->params = [
            'chat_id' => $this->chatId,
            'title' => $input['title']
        ];
    }

    public function setChatDescription($input = [])
    {
        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatDescription';
        $this->params = [
            'chat_id' => $this->chatId,
            'description' => $input['description']
        ];
    }


    public function setPinChatMessage($input = [])
    {
        $this->userId = $input['user_id'] ?? $this->userId;
        $this->messageId = $input['message_id'] ?? $this->messageId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/pinChatMessage';
        $this->params = [
            'chat_id' => $this->chatId,
            'message_id' => $this->messageId
        ];
        $this->addContentsToFile('errors.txt', $this->params);

        $this->setDisableStatusAndReply($input['disable_notification'], null);
    }


    public function setUnPinChatMessage($input = [])
    {
        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/pinChatMessage';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function setLeaveChat($input = [])
    {
        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/leaveChat';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function getChat()
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getChat';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function getChatAdministrators()
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getChatAdministrators';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function getChatMembersCount()
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getChatMembersCount';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function getChatMember($input)
    {

        $this->userId = $input['user_id'] ?? $this->userId;
        $this->chatId = $input['chat_id'] ?? $this->chatId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getChatMember';
        $this->params = [
            'chat_id' => $this->chatId,
            'user_id' => $this->userId
        ];
    }

    public function setChatStickerSet($input)
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setChatStickerSet';
        $this->params = [
            'chat_id' => $this->chatId,
            'sticker_set_name' => $input['sticker_set_name']
        ];
    }

    public function deleteChatStickerSet($input)
    {

        $this->userId = $input['user_id'] ?? $this->userId;

        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/deleteChatStickerSet';
        $this->params = [
            'chat_id' => $this->chatId,
        ];
    }

    public function answerCallbackQuery($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/answerCallbackQuery';
        $this->params = [
            'callback_query_id' => $this->callbackId,
            'text' => $input['text'],
            'show_alert' => $input['show_alert'],
            'url' => $input['url'],
            'cache_time' => $input['cache_time']
        ];
    }

    public function editMessageText($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/editMessageText';
        $this->params = [
            'chat_id' => $input['chat_id'],
            'message_id' => $input['message_id'],
            'inline_message_id' => $input['inline_message_id'],
            'text' => $input['text'],
            'parse_mode' => $input['parse_mode'],
            'disable_web_page_preview' => $input['disable_web_page_preview']
        ];

        $this->setButtons($input['buttons']);
    }

    public function getMyCommands()
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/getMyCommands';
    }

    public function setMyCommands($input)
    {
        $this->url = "https://api.telegram.org/bot" . __API_TOKEN__ . '/setMyCommands';
        $this->params = [
            'commands' => json_encode($input['commands'])
        ];
    }

    // set properties methods
    private function setDisableStatusAndReply($disableNot, $reply): void
    {
        if ($disableNot) {
            $this->params['disable_notification'] = true;
        }

        if ($reply) {
            $this->params['reply_to_message_id'] = $this->messageId;
        }
    }

    private function setDuration($duration)
    {
        if ($duration) {
            $this->params['duration'] = $duration;
        }

    }

    private function setStreamingMod($StreamingMod)
    {
        if ($StreamingMod) {
            $this->params['supports_streaming'] = $StreamingMod;
        }

    }

    private function setThumbnail($thumb): void
    {
        if ($thumb) {
            $this->params['thumb'] = $thumb;
        }
    }

    private function setPerformer($performer)
    {
        if ($performer) {
            $this->params['performer'] = $performer;
        }
    }

    private function setTitle($title)
    {
        if ($title) {
            $this->params['title'] = $title;
        }
    }

    private function setLength($length)
    {
        if ($length) {
            $this->params['length'] = $length;
        }
    }

    private function setLivePeriod($livePeriod)
    {
        if ($livePeriod) {
            $this->params['live_period'] = $livePeriod;
        }
    }

    private function setAddress($address)
    {
        if ($address) {
            $this->params['address'] = $address;
        }
    }

    private function setFoursquare($foursquareId, $foursquareType)
    {
        if ($foursquareId) {
            $this->params['foursquare_id'] = $foursquareId;
        }
        if ($foursquareType) {
            $this->params['foursquare_type'] = $foursquareType;
        }
    }

    private function setLastName($lastname)
    {
        if ($lastname) {
            $this->params['last_name'] = $lastname;
        }
    }


    // Initial methods ....

    function sendCurlRequest()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        var_dump($this->url, $this->params);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
//        var_dump($result);
        return $result;
    }

    function sendCurlFile()
    {

    }

    public function getUserContents($filename = 'php://input', $flag = null, $fileType = null)
    {
        $input = json_decode(file_get_contents($filename), $flag);
        if ($fileType != null) {
            $input = json_decode($filename, $flag);
        }
        return $input;
    }


    public function setUserData($input): void
    {
        if (array_key_exists('message', $input)) {
            $this->userId = $input['message']['from']['id'];
            $this->messageId = $input['message']['message_id'];
            $this->userName = $input['message']['from']['username'] ?? null;
            $this->firstName = $input['message']['from']['first_name'];
            $this->lastName = $input['message']['from']['last_name'] ?? null;
            $this->chatId = $input['message']['chat']['id'];
            $this->command = $input['message']['text'];
            $this->userPhone = $input['message']['contact']['phone_number'] ?? null;
            $this->userLongitude = $input['message']['location']['longitude'] ?? null;
            $this->userLatitude = $input['message']['location']['latitude'] ?? null;
        } elseif (array_key_exists('callback_query', $input)) {
            $this->command = $input['callback_query']['data'];
            $this->userId = $input['callback_query']['from']['id'];
            $this->messageId = $input['callback_query']['message_id'];
            $this->chatId = $input['callback_query']['chat']['id'];
            $this->userName = $input['callback_query']['from']['username'] ?? null;
            $this->firstName = $input['callback_query']['from']['first_name'];
            $this->lastName = $input['callback_query']['from']['last_name'] ?? null;
            $this->callbackId = $input['callback_query']['id'];
        }

    }


    public function addContentsToFile($fileName, $input, $flag = null): void
    {
        file_put_contents($fileName, $input, $flag);
    }

    private function setvCard($vcard)
    {
        if ($vcard) {
            $this->params['vcard'] = $vcard;
        }
    }

    private function setAnonymus($anonymous)
    {
        if ($anonymous) {
            $this->params['anonymous'] = true;
        }
    }

    private function setType($type)
    {
        if ($type) {
            $this->params['type'] = ($type);
        }
    }

    private function setallowsMultipleAnswers($allows_multiple_answers)
    {
        if ($allows_multiple_answers) {
            $this->params['allows_multiple_answers'] = $allows_multiple_answers;
        }
    }

    private function setCorrectOptionId($correctOptionId)
    {
        if ($correctOptionId) {
            $this->params['correct_option_id'] = $correctOptionId;
        }
    }

    private function setIsClosed($isClosed)
    {
        if ($isClosed) {
            $this->params['is_closed'] = true;
        }
    }

    private function setLimit($limit)
    {
        if ($limit) {
            $this->params['limit'] = $limit;
        }
    }

    private function setOffset($offset)
    {
        if ($offset) {
            $this->params['offset'] = $offset;
        }
    }

    private function setUntil_date($until_date)
    {
        if ($until_date) {
            $this->params['until_date'] = $until_date;
        }
    }

    private function setChatPermissions($permissions)
    {
        $this->params['permissions'] = json_encode($permissions);
    }

    private function setCanChangInfo($can_change_info)
    {
        if ($can_change_info) {
            $this->params['can_change_info'] = true;
        }
    }

    private function setCanPostMessages($can_post_messages)
    {
        if ($can_post_messages) {
            $this->params['can_post_messages'] = true;
        }
    }

    private function setCanEditMessages($can_edit_messages)
    {
        if ($can_edit_messages) {
            $this->params['can_edit_messages'] = true;

        }
    }

    private function setCanDeleteMessages($can_delete_messages)
    {
        if ($can_delete_messages) {
            $this->params['can_delete_messages'] = true;

        }
    }

    private function setCanInviteUsers($can_invite_users)
    {
        if ($can_invite_users) {
            $this->params['can_invite_users'] = true;

        }
    }

    private function setCanRestrictMembers($can_restrict_members)
    {
        if ($can_restrict_members) {
            $this->params['can_restrict_members'] = true;
        }
    }

    private function setCanPinMessages($can_pin_messages)
    {
        if ($can_pin_messages) {
            $this->params['can_pin_messages'] = true;
        }
    }

    private function setCanPromoteMembers($can_promote_members)
    {
        if ($can_promote_members) {
            $this->params['can_promote_members'] = true;
        }
    }

    private function setButtons($buttons)
    {
        if ($buttons) {
            $this->params['reply_markup'] = json_encode($buttons);
        }
    }
}