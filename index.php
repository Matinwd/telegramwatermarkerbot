<?php
require_once "src/MyTelegramSender.php";
require_once "src/gdfa.php";

$tg = new \Src\MyTelegramSender();

if ($tg->command == '/start') {
    $userCheck = checkIfUserExists($tg);
    if ($userCheck == 0) {
        $user = addUserToDb($tg);
    } else {
        $res = changeStatus($tg, 'home');
    }

    $tg->setText(['text' => 'سلام خوش امدید. ', 'buttons' => [
        'keyboard' => [
            [
                'نوشتن متن روی عکس'
            ]
        ], 'keyboard_resize' => true
    ]]);
    $err = $tg->sendCurlRequest();
    $tg->addContentsToFile('errors.txt', print_r($err, true), FILE_APPEND);
}

if ($tg->command == 'نوشتن متن روی عکس') {
    changeStatus($tg, 'sendStart');
}

if (getStatus($tg) == 'sendStart') {
    changeStatus($tg, 'sendType');
    $tg->setText(['text' => 'لطفا نوع ملک را بفرستید', 'buttons' =>
        ['keyboard' => [
            [
                'ویلایی', 'اداری و تجاری', 'مسکونی', 'زمین و باغ'
            ]
        ], 'keyboard_resize' => true]
    ]);
    $tg->sendCurlRequest();
    die();
}

if (getStatus($tg) == 'sendType') {
    $valid = inputValidation($tg->command, [
        'ویلایی', 'اداری و تجاری', 'مسکونی', 'زمین و باغ'
    ]);
    if (!$valid) {
        $tg->setText(['text' => 'لطفا از دکمه ها استفاده کنید...']);
        $tg->sendCurlRequest();
        $tg->addContentsToFile('err.txt', 'Hel');
        die();
    }
    changeStatus($tg, 'sendTitle');
    $tg->setText(['text' => 'لطفا تیتر ملک رو بفرستید', 'buttons' => [
        'remove_keyboard' => true
    ]]);
    $tg->sendCurlRequest();
    $image = 'http://s12.picofile.com/file/8398483050/init.jpg';
    $image = makeImage($tg->command, $image, 2200, 350);
    $res = saveImage($tg, $image);
    die();
}

/*if (getStatus($tg) == 'sendCode') {
    changeStatus($tg, 'sendTitle');
    $tg->setText(['text' => 'لطفا تیتر ملک رو بفرستید', 'buttons' =>
        ['remove_keyboard' => true
        ]
    ]);
    $tg->sendCurlRequest();
    $oldImage = getIMage($tg);
    $image = makeImage($tg->command, $oldImage, 2200, 650);
    unlink($oldImage);
    clearstatcache();
    saveImage($tg, $image);
    die();
}*/

if (getStatus($tg) == 'sendTitle') {
    changeStatus($tg, 'sendMeter');
    $tg->setText(['text' => 'لطفا متراژ ملک رو بفرستید', 'buttons' =>
        ['keyboard' => [
            [
                'متراژ زیر 100 متر', 'متراژ 100 تا 500 متر', 'متراژ 500 تا 1000 متر', 'متراژ بالای 1000 متر', 'متراژ بالای 2000 متر'
            ]
        ]
        ]
    ]);
    $tg->sendCurlRequest();
    $oldImage = getIMage($tg);
    $image = makeImage($tg->command, $oldImage, 500, 1250);
    unlink($oldImage);
    clearstatcache();
    saveImage($tg, $image);
    die();
}


if (getStatus($tg) == 'sendMeter') {
    $valid = inputValidation($tg->command, [
        'متراژ زیر 100 متر', 'متراژ 100 تا 500 متر', 'متراژ 500 تا 1000 متر', 'متراژ بالای 1000 متر', 'متراژ بالای 2000 متر'
    ]);
    if (!$valid) {
        $tg->setText(['text' => 'لطفا از دکمه ها استفاده کنید...']);
        $tg->sendCurlRequest();
        $tg->addContentsToFile('err.txt', 'Hel');
        die();
    }
    changeStatus($tg, 'sendDocument');
    $tg->setText(['text' => 'لطفا عرض ملک رو بفرستید', 'buttons' =>
        ['keyboard' => [
            [
                'عرض زیر 100 متر', 'عرض 100 تا 500 متر', 'عرض 500 تا 1000 متر', 'عرض بالای 1000 متر', 'عرض بالای 2000 متر '
            ]
        ]
        ]]);
    $tg->sendCurlRequest();
    $oldImage = getIMage($tg);
    $image = makeImage($tg->command, $oldImage, 2200, 950);
    unlink($oldImage);
    clearstatcache();
    saveImage($tg, $image);
    die();
}

if (getStatus($tg) == 'sendDocument') {
    $valid = inputValidation($tg->command, [
        'عرض زیر 100 متر', 'عرض 100 تا 500 متر', 'عرض 500 تا 1000 متر', 'عرض بالای 1000 متر', 'عرض بالای 2000 متر '
    ]);
    if (!$valid) {
        $tg->setText(['text' => 'لطفا از دکمه ها استفاده کنید...']);
        $tg->sendCurlRequest();
        $tg->addContentsToFile('err.txt', 'Hel');
        die();
    }
    changeStatus($tg, 'sendCount');
    $tg->setText(['text' => 'لطفا سند ملک رو مشخص کنید', 'buttons' =>
        ['keyboard' => [
            [
                'سند 6 دانگ', 'سند 3 دانگ',
            ]
        ]
        ]]);
    $tg->sendCurlRequest();
    $oldImage = getImage($tg);
    $image = makeImage($tg->command, $oldImage, 2200, 1250);
    unlink($oldImage);
    clearstatcache();
    saveImage($tg, $image);
    die();
}

if (getStatus($tg) == 'sendCount') {
    $valid = inputValidation($tg->command, [
        'سند 6 دانگ', 'سند 3 دانگ',
    ]);
    if (!$valid) {
        $tg->setText(['text' => 'لطفا از دکمه ها استفاده کنید...']);
        $tg->sendCurlRequest();
        $tg->addContentsToFile('err.txt', 'Hel');
        die();
    }
    changeStatus($tg, 'theEnd');
    $tg->setText(['text' => 'لطفا تعداد اتاق رو مشخص کنید', 'buttons' =>
        ['keyboard' => [
            [
                'تک اتاق', '2 اتاق', '3 اتاق', '4 اتاق', 'بالای 4 اتاق'
            ]
        ], 'keyboard_resize' => true
        ]]);
    $tg->sendCurlRequest();
    $oldImage = getImage($tg);
    $image = makeImage($tg->command, $oldImage, 2200, 1550);
    unlink($oldImage);
    clearstatcache();
    saveImage($tg, $image);
    die();
}

if (getStatus($tg) == 'theEnd') {
    $valid = inputValidation($tg->command, [
        'تک اتاق', '2 اتاق', '3 اتاق', '4 اتاق', 'بالای 4 اتاق'
    ]);
    if (!$valid) {
        $tg->setText(['text' => 'لطفا از دکمه ها استفاده کنید...']);
        $tg->sendCurlRequest();
        $tg->addContentsToFile('err.txt', 'Hel');
        die();
    }
    $oldImage = getImage($tg);
    $image = makeImage($tg->command, $oldImage, 2200, 650);
    saveImage($tg, $image);
    unlink($oldImage);
    clearstatcache();
    $image = getImage($tg);
    $bot_url = "https://api.telegram.org/bot" . __API_TOKEN__ . "/";
    $url = $bot_url . "sendPhoto";
    $tg->setText(['text' => 'عکس درحال پردازش است..',
        'buttons' => [
            'keyboard' => [
                [
                    'نوشتن متن روی عکس'
                ]
            ], 'keyboard_resize' => true
        ]
    ]);
    $tg->sendCurlRequest();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        "chat_id" => $tg->chatId,
        "photo" => new CURLFile(realpath($image))
    ));
    saveLastImage($tg, $image);
    clearUserImage($tg);
    $output = curl_exec($ch);
    $tg->addContentsToFile('errors.txt', $image . PHP_EOL);
    $tg->addContentsToFile('errors.txt', $output, FILE_APPEND);
}

function inputValidation($input, array $whiteList)
{
    if (in_array($input, $whiteList)) {
        return true;
    }
    return false;
}

function reversePersianNumber($number)
{
    $number = (string)$number;
    $numbers = str_split($number);
    $newNumber = array_reverse($numbers);
    return implode($newNumber);
}

function checkEnNumber($number)
{
    $res = preg_match('/[1-9]/', $number, $matches);
    return $res ? true : false;
}

function saveLastImage(\Src\MyTelegramSender $tg, $image)
{
    if (!dir('images')) {
        mkdir('images');
    }
    $newImage = 'images/' . md5(rand(1000, 100000)) . '.jpg';
    copy($image, $newImage);
    $tg->db->query("INSERT INTO `images` (`image`) values (:image) ");
    $tg->db->bindValue('image', $newImage);
    $tg->db->execute();
}

function clearUserImage(\Src\MyTelegramSender $tg)
{
    $tg->db->query('UPDATE users SET user_photo = :user_photo where user_id = :user_id');
    $tg->db->bindValue('user_photo', null);
    $tg->db->bindValue('user_id', $tg->userId);
    $tg->db->execute();
}

function saveImage(\Src\MyTelegramSender $tg, $image)
{
    $tg->db->query("UPDATE users SET user_photo = :user_photo WHERE user_id = :user_id");
    $tg->db->bindValue('user_photo', $image);
    $tg->db->bindValue('user_id', $tg->userId);
    $res = $tg->db->execute();
    return $res ? $image : false;

}

function getImage(\Src\MyTelegramSender $tg)
{
    $tg->db->query("SELECT user_photo from `users` WHERE user_id = :user_id");
    $tg->db->bindValue('user_id', $tg->userId);
    $tg->db->execute();
    $res = $tg->db->resultOne();
    if ($res->user_photo) {
        return $res->user_photo;
    }
    return false;
}

function checkEnText($text, $font)
{

    if (preg_match('/[^a-zA-Z0-9\s]+/', $text)) {
        $text = fagd($text, 'fa', 'C:\Users\Matinwd\Desktop\Vazir-FD-WOL.ttf');
        $font = 'C:\Users\Matinwd\Desktop\Vazir-FD-WOL.ttf';
    }
    return [$text, $font];
}

function makeImage($text, $image, $x, $y): string
{
    $img = imagecreatefromjpeg($image);
    $colors = imagecolorallocate($img, 255, 255, 255);
    $font = 'C:\Windows\Fonts\arial.ttf';
    $font = 'C:\Users\Matinwd\Desktop\Vazir-FD-WOL.ttf';
    list($text, $font) = checkEnText($text, $font);
    imagettftext($img, 100, 0, $x, $y, $colors, $font, $text);

    if (!dir('pictures')) {
        mkdir('pictures');
    }
    header('Content-type: image/jpeg');
    $fileAddress = 'pictures/' . md5(rand(11, 121111) . md5(random_int(1, 10000))) . '.jpg';
    imagejpeg($img, $fileAddress);
    imagedestroy($img);
    return $fileAddress;
}

function changeStatus(\Src\MyTelegramSender $tg, string $status): bool
{
    $tg->addContentsToFile('errors.txt', $status, FILE_APPEND);
    $tg->addContentsToFile('log.txt', $status, FILE_APPEND);
    $tg->db->query("UPDATE users SET step = :step WHERE user_id = :user_id");
    $tg->db->bindValue('step', $status);
    $tg->db->bindValue('user_id', $tg->userId);
    $res = $tg->db->execute();
    return $res;
}

function getStatus(\Src\MyTelegramSender $tg): string
{
    $tg->db->query("SELECT step FROM `users` WHERE user_id = :user_id");
    $tg->db->bindValue('user_id', $tg->userId);
    $tg->db->execute();
    $res = $tg->db->resultOne();
    return $res->step;
}

function addUserToDb(\Src\MyTelegramSender $tg): bool
{
    $tg->db->query("INSERT INTO `users` (`user_id`,`name`,`step`,`password`) VALUES (:user_id,:name,:step,:password)");
    $tg->db->bindValue('user_id', $tg->userId);
    $tg->db->bindValue('name', $tg->firstName);
    $tg->db->bindValue('step', 'home');
    $tg->db->bindValue('password', md5(rand(1000, 2000000)));
    $res = $tg->db->execute();
    return $res;
}

function checkIfUserExists(\Src\MyTelegramSender $tg): bool
{
    $tg->db->query("SELECT * FROM `users` where `user_id` = :user_id");
    $tg->db->bindValue('user_id', $tg->userId);
    $tg->db->execute();
    $count = $tg->db->count();
    return $count;
}
